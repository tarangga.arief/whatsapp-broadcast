from time import sleep
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import re


def get_wait_element(browser, time, xpath):
    return WebDriverWait(browser, time).until(
        EC.presence_of_element_located((By.XPATH, xpath))
    )


def get_group(browser, group_name):
    search = get_wait_element(browser, 20, '//div[@id="side"]//div[@contenteditable="true"][@data-tab=3]')
    search.clear()
    search.send_keys(group_name)
    sleep(0.5)
    group = get_wait_element(browser, 3, "//span[text()='{name}']".format(name=group_name))
    group.click()


def get_num_participant(browser):
    header = get_wait_element(browser, 3, '//header[@class="_1-qgF"]')
    header.click()
    participant = get_wait_element(browser, 10, '//div[@class="_2zCWg"]')
    get_wait_element(browser, 3, '//span[@data-testid="x"][@data-icon="x"]').click()
    return int(re.search(r'>(\d+) participants?', participant.get_attribute('innerHTML')).group(1))


def start_forward(browser):
    AC = ActionChains(browser)
    el = browser.find_element_by_xpath('//header[@class="_1-qgF"]//span[@data-testid="menu"][@data-icon="menu"]')
    AC.move_to_element(el)
    AC.click().send_keys(Keys.DOWN + Keys.DOWN + Keys.ENTER).perform()
    AC.reset_actions()


def scroll_until(browser, until):
    data = browser.find_elements_by_class_name('GDTQm')
    scroll = True
    for d in data:
        scroll = not d.text in until

    to_scroll = get_wait_element(browser, 2, '//div[@data-tab="7"]')

    while scroll:
        to_scroll.send_keys(Keys.PAGE_UP)
        scroll = not browser.find_element_by_class_name('GDTQm').text in until
        sleep(0.15)


def tag_group(browser, num, text):
    send_box = get_wait_element(browser, 5, '//div[@id="main"]//div[@contenteditable="true"][@data-tab="6"]')
    send_box.clear()
    AC = ActionChains(browser)
    AC.move_to_element(send_box)
    AC.send_keys(text)
    AC.send_keys(' @')
    AC.send_keys(Keys.ENTER)
    for i in range(1, num - 1):
        AC.send_keys('@')
        for j in range(0, i):
            AC.send_keys(Keys.DOWN)
        AC.send_keys(Keys.ENTER)
    AC.send_keys(Keys.ENTER)
    AC.perform()


def tag_groups(browser, groups, text):
    for g in groups:
        get_group(browser, g)
        sleep(0.5)
        tag_group(browser, get_num_participant(browser), text)
        sleep(0.5)


def forward_content(browser, to):
    get_wait_element(browser, 5, '//span[@data-testid="forward"][@data-icon="forward"]').click()
    search_box = get_wait_element(browser, 3, '//div[contains(@class, "OMoBQ") and contains(@class, "_3wXwX")]')
    for group_name in to:
        search_box.find_element_by_xpath('.//div[@contenteditable="true"][@data-tab="3"]').send_keys(group_name)
        sleep(0.5)
        search_box.find_element_by_xpath('.//span[text()="{name}"]'.format(name=group_name)).click()
    sleep(0.25)
    search_box.find_element_by_xpath('.//span[@data-testid="send"]').click()


def search_content(element_list):
    pos = [-1, -1]
    for i in range(len(element_list) - 1, 0, -1):
        try:
            curr_text = element_list[i].find_element_by_class_name('_3ExzF').text
        except Exception as e:
            continue

        if curr_text == '#TERKIRIM#':
            return pos, None

        if curr_text == '###':
            pos[1] = i
        re_text = re.match(r'### +(\w+)( *.*)', curr_text)
        if re_text:
            pos[0] = i

            return pos, [re_text.group(1), re_text.group(2)]



