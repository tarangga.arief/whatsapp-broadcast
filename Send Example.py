from selenium import webdriver
from WhatsappLib import *

PATH = <YOUR PATH>
AUTH_PATH = <AUTH PATH>

options = webdriver.ChromeOptions()
options.add_argument(AUTH_PATH)

groups = <GROUPS> # list of groups to sent broadcast
browser = webdriver.Chrome(
    executable_path=PATH, options=options)

browser.maximize_window()

browser.get('https://web.whatsapp.com/')

source = <SOURCE GROUPS> #group that contain broadcast
get_group(browser, source)
start_forward(browser)
scroll_until(browser, ['YESTERDAY'])
data = browser.find_elements_by_class_name('GDTQm')

pos, content = search_content(data)

for i in range(pos[0]+1, pos[1]):
    data[i].find_element_by_xpath('.//div[@aria-checked="false"][@role="checkbox"]').click()

forward_content(browser, groups[content[0].lower()])
tag_groups(browser, groups[content[0].lower()], content[1])
get_group(browser, source)

